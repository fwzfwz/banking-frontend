import React from 'react';
import { Route, BrowserRouter, Switch, Redirect } from 'react-router-dom';
import Header from './components/layouts/Header';
import LoggedRoute from './components/routes/LoggedRoute';
import PublicRoute from './components/routes/PublicRoute';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <PublicRoute path="/login" component={Login} />
          <PublicRoute path="/register" component={Register} />
          <LoggedRoute path="/home" component={Home} />
          <Route path="/">
            <Redirect to="/home" />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
