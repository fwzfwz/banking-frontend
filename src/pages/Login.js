import React from 'react';
import { Container, Form, Col, Row, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const Login = () => {
  const [userId, setUserId] = React.useState();
  const [userPin, setUserPin] = React.useState();
  const [isLoading, setLoading] = React.useState();

  const history = useHistory();

  const doLogin = e => {
    e.preventDefault();
    setLoading(true);

    fetch('http://localhost:8080/login/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ userId: userId, pin: userPin }),
    })
      .then(resp => {
        if (resp.status == '202') {
          return resp.json();
        } else if (resp.status == '404') {
          alert('Wrong User ID or Pin');
        } else {
          alert('An error occured');
        }
      })
      .then(resp => {
        console.log(resp);
        if (resp) {
          sessionStorage.setItem('userId', userId);
          sessionStorage.setItem('token', resp.token);
          history.push('/home');
          window.location.reload();
        }
        setLoading(false);
      });
  };

  return (
    <Container>
      <Form className="mx-auto w-50 my-5 py-5" onSubmit={e => doLogin(e)}>
        <Form.Group as={Row} controlId="formUserId">
          <Col lg={{ span: 2, offset: 2 }} className="pt-2">
            <Form.Label>User ID</Form.Label>
          </Col>
          <Col sm="6">
            <Form.Control number onChange={e => setUserId(e.target.value)} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formUserPin" className="my-1">
          <Col lg={{ span: 2, offset: 2 }} className="pt-2">
            <Form.Label>Web PIN</Form.Label>
          </Col>
          <Col sm="6">
            <Form.Control
              type="password"
              onChange={e => setUserPin(e.target.value)}
            />
          </Col>
        </Form.Group>
        <Row>
          <Col lg={{ span: 2, offset: 4 }} className="pt-2">
            <Button
              variant="primary"
              type="submit"
              disabled={isLoading ? true : false}>
              {isLoading ? '...' : 'Login'}
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};

export default Login;
