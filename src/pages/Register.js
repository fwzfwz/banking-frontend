import React from 'react';
import { Container, Form, Col, Row, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';

const Register = () => {
  const [noRekening, setNoRekening] = React.useState();
  const [userId, setUserId] = React.useState();
  const [pin, setPin] = React.useState();
  const history = useHistory();

  React.useEffect(() => {
    if (sessionStorage.getItem('userId')) {
      return <Redirect to={'/home'} />;
    }
  });

  const doRegister = e => {
    fetch('http://localhost:8080/register/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ noRekening, userId, pin }),
    }).then(resp => {
      if (resp.status == '201') {
        history.push('/login');
      } else if (resp.status == '404') {
        alert('User Already Exist');
      } else {
        alert('An error occured');
      }
    });
  };

  return (
    <Container>
      <Form className="mx-auto w-50 my-5 py-5">
        <Form.Group as={Row} controlId="formNoRekening" className="my-1">
          <Col lg={{ span: 3, offset: 1 }} className="pt-2">
            <Form.Label>No. Rekening</Form.Label>
          </Col>
          <Col sm="6">
            <Form.Control
              type="number"
              onChange={e => setNoRekening(e.target.value)}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formUserId" className="my-1">
          <Col lg={{ span: 3, offset: 1 }} className="pt-2">
            <Form.Label>UserID</Form.Label>
          </Col>
          <Col sm="6">
            <Form.Control onChange={e => setUserId(e.target.value)} />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formUserPin" className="my-1">
          <Col lg={{ span: 3, offset: 1 }} className="pt-2">
            <Form.Label>Web PIN</Form.Label>
          </Col>
          <Col sm="6">
            <Form.Control
              type="password"
              onChange={e => setPin(e.target.value)}
            />
          </Col>
        </Form.Group>
        <Row>
          <Col lg={{ span: 2, offset: 4 }} className="pt-2">
            <Button variant="primary" onClick={() => doRegister()}>
              Register
            </Button>
          </Col>
        </Row>
      </Form>
    </Container>
  );
};

export default Register;
