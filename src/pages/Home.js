import React from 'react';
import TableRow from '../components/contents/TableRow';
import { Form, Container, Row, Col } from 'react-bootstrap';
import moment from 'moment';

const Home = () => {
  const [transactions, setTransactions] = React.useState([]);
  const [nama, setNama] = React.useState();
  const [noRekening, setNoRekening] = React.useState();
  const [saldo, setSaldo] = React.useState();
  const [upperRange, setUpperRange] = React.useState(
    moment().add(7, 'hours').format('yyyy-MM-DD hh:mm:ss')
  );
  const [lowerRange, setlowerRange] = React.useState(
    moment().subtract(30, 'days').add(7, 'hours').format('yyyy-MM-DD hh:mm:ss')
  );

  React.useEffect(() => {
    pullData();
  }, [upperRange, lowerRange]);

  const pullData = async () => {
    await fetch('http://localhost:8080/transaksi/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: sessionStorage.getItem('token'),
      },
      body: JSON.stringify({
        upperRange,
        lowerRange,
      }),
    }).then(resp => {
      if (resp.status === 200) {
        resp.json().then(resp => {
          setTransactions(resp.data);
        });
      } else if (resp.status === 401) {
        sessionStorage.removeItem('token');
        window.location.reload();
      }
    });
    await fetch('http://localhost:8080/rekening/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: sessionStorage.getItem('token'),
      },
      body: JSON.stringify({}),
    }).then(resp => {
      if (resp.status === 200) {
        resp.json().then(resp => {
          setNama(resp.fullname);
          setNoRekening(resp.noRekening);
          setSaldo(resp.saldo);
        });
      } else if (resp.status === 401) {
        sessionStorage.removeItem('token');
        window.location.reload();
      }
    });
  };

  return (
    <Container className="home p-5">
      <Row className="pb-5">
        <Row className="col-8">
          <Col lg={4}>
            <h5>
              <b>Fullname</b>
            </h5>
            <h5>
              <b>UserID</b>
            </h5>
            <h5>
              <b>No. Rekening</b>
            </h5>
            <h5>
              <b>Saldo</b>
            </h5>
          </Col>
          <Col lg={8}>
            <h5>{nama}</h5>
            <h5>{sessionStorage.getItem('userId')}</h5>
            <h5>{noRekening}</h5>
            <h5>{saldo}</h5>
          </Col>
        </Row>
      </Row>

      {transactions.length < 1 ? (
        <h1 className="text-center">Riwayat Transaksi Kosong</h1>
      ) : (
        <div>
          <Container>
            <Row>
              <Col md={{ span: 6, offset: 6 }}>
                <Row>
                  <Form>
                    <Form.Group as={Row}>
                      <Form.Label column sm="3">
                        Range
                      </Form.Label>
                      <Col sm="9">
                        <Form.Control
                          type="date"
                          onChange={e => {
                            setlowerRange(
                              moment(e.target.value)
                                .add(7, 'hours')
                                .format('yyyy-MM-DD hh:mm:ss')
                            );
                          }}
                        />
                      </Col>
                    </Form.Group>
                  </Form>
                  <Form className="ml-3">
                    <Form.Group as={Row}>
                      <Form.Label column sm="2">
                        To
                      </Form.Label>
                      <Col sm="10">
                        <Form.Control
                          type="date"
                          onChange={e => {
                            setUpperRange(
                              moment(e.target.value)
                                .add(7, 'hours')
                                .format('yyyy-MM-DD hh:mm:ss')
                            );
                          }}
                        />
                      </Col>
                    </Form.Group>
                  </Form>
                </Row>
              </Col>
            </Row>
          </Container>
          <div style={{ overflow: 'auto', maxHeight: '95vh' }}>
            <TableRow transaksi={transactions} />
          </div>
        </div>
      )}
    </Container>
  );
};

export default Home;
