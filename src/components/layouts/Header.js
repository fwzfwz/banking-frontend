import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import TransferModal from '../contents/TransferModal';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const Header = () => {
  const history = useHistory();

  const doLogout = () => {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userId');
    window.location.reload();
  };

  return (
    <>
      <Navbar bg="light" variant="light">
        <Navbar.Brand>Bank ABC</Navbar.Brand>
        {sessionStorage.getItem('token') ? (
          <>
            <Nav className="mr-auto">
              <Nav.Link>
                <TransferModal />
              </Nav.Link>
            </Nav>
            <Form inline>
              <Button variant="outline-primary" onClick={() => doLogout()}>
                Logout
              </Button>
            </Form>
          </>
        ) : (
          <>
            <Nav className="mr-auto">
              <Nav.Link>
                <Link to="/register" className="text-secondary">
                  Register
                </Link>
              </Nav.Link>
            </Nav>
          </>
        )}
      </Navbar>
    </>
  );
};

export default Header;
