import React from 'react';
import { Modal, Form, Row, Col, InputGroup, Button } from 'react-bootstrap';

const TransferModal = () => {
  const [show, setShow] = React.useState(false);
  const [kodeBank, setKodeBank] = React.useState('002');
  const [rekeningPenerima, setRekeningPenerima] = React.useState();
  const [amount, setAmount] = React.useState();
  const [berita, setBerita] = React.useState();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const postTransfer = () => {
    fetch('http://localhost:8080/transfer/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: sessionStorage.getItem('token'),
      },
      body: JSON.stringify({
        receiverNoRekening: rekeningPenerima,
        receiverVendorId: kodeBank,
        amount,
        berita,
      }),
    });
    window.location.reload();
  };

  return (
    <>
      <a onClick={handleShow}>Transfer Antar Bank</a>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        size="lg">
        <Modal.Header>
          <Modal.Title>Transfer Antar Bank</Modal.Title>
        </Modal.Header>
        <Modal.Body className="show-grid">
          <Form>
            <Form.Group as={Row} controlId="formNoRekeningPenerima">
              <Form.Label column sm="3">
                Kode Bank
              </Form.Label>
              <Col sm="9">
                <Form.Control
                  as="select"
                  onClick={e => setKodeBank(e.target.value)}>
                  <option value="002">002 - Bank BAC</option>
                  <option value="003">003 - Bank CBA</option>
                </Form.Control>
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formNoRekeningPenerima">
              <Form.Label column sm="3">
                Rekening Penerima
              </Form.Label>
              <Col sm="9">
                <Form.Control
                  type="number"
                  placeholder="Nomor Rekening Penerima Transfer"
                  value={rekeningPenerima}
                  onChange={e => setRekeningPenerima(e.target.value)}
                />
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formNoRekeningPenerima">
              <Form.Label column sm="3">
                Jumlah Transfer
              </Form.Label>
              <Col sm="9">
                <InputGroup>
                  <InputGroup.Prepend>
                    <InputGroup.Text>Rp.</InputGroup.Text>
                  </InputGroup.Prepend>
                  <Form.Control
                    type="number"
                    placeholder="Jumlah Uang Yang Akan Di Transfer"
                    value={amount}
                    onChange={e => setAmount(e.target.value)}
                  />
                </InputGroup>
              </Col>
            </Form.Group>
            <Form.Group as={Row} controlId="formNoRekeningPenerima">
              <Form.Label column sm="3">
                Berita
              </Form.Label>
              <Col sm="9">
                <Form.Control
                  type="text"
                  value={berita}
                  onChange={e => setBerita(e.target.value)}
                />
              </Col>
            </Form.Group>
            <Button
              className="mx-1"
              variant="outline-primary"
              onClick={() => postTransfer()}>
              Transfer
            </Button>
            <Button
              className="mx-1"
              variant="outline-danger"
              onClick={handleClose}>
              Cancel
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default TransferModal;
