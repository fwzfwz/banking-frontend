import React from 'react';
import { Table } from 'react-bootstrap';
import TableCell from '../elements/TableCell';
import RiwayatTransferModal from './RiwayatTransferModal';

const HistoryTable = props => {
  const [transactions, setTransactions] = React.useState([]);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [modalData, setModalData] = React.useState({
    senderNoRekening: '',
    senderVendorId: '',
    receiverNoRekening: '',
    receiverVendorId: '',
    amount: '',
    status: '',
    timestamp: '',
    berita: '',
  });

  React.useEffect(() => setTransactions(props.transaksi));

  return (
    <Table striped bordered hover over>
      <thead>
        <tr>
          <th>Tanggal Transaksi</th>
          <th>Rekening Pengirim</th>
          <th>Rekening Penerima</th>
          <th>Jumlah</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        {transactions.map(row => (
          <tr
            onClick={() => {
              setModalVisible(true);
              setModalData(row);
            }}
            key={row.transaksiId}>
            <RiwayatTransferModal
              isShow={modalVisible}
              setIsShow={setModalVisible}
              data={modalData}
            />
            <TableCell content={new Date(row.timestamp).toDateString()} />
            <TableCell content={row.senderNoRekening} />
            <TableCell content={row.receiverNoRekening} />
            <TableCell content={'Rp. ' + row.amount} />
            {row.status === 'SUCCESS' ? (
              <TableCell content={row.status} classes="text-success" />
            ) : (
              <TableCell content={row.status} classes="text-danger" />
            )}
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default HistoryTable;
