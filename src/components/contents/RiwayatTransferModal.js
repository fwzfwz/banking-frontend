import React from 'react';
import { Modal, Container, Row, Col } from 'react-bootstrap';

const TransferModal = ({ isShow, setIsShow, data }) => {
  const handleClose = () => setIsShow(false);

  return (
    <Modal show={isShow} onHide={handleClose} size="lg">
      <Modal.Header>
        <Modal.Title>Rincian Transfer</Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid">
        <Container>
          <Row>
            <Col lg={4}>
              <h5>Rekening Pengirim</h5>
            </Col>
            <Col lg={8}>
              <h5>{data.senderNoRekening}</h5>
            </Col>
          </Row>
          <Row>
            <Col lg={4}>
              <h5>Rekening Penerima</h5>
            </Col>
            <Col lg={8}>
              <h5>{data.receiverNoRekening}</h5>
            </Col>
          </Row>
          <Row>
            <Col lg={4}>
              <h5>Jumlah Transfer</h5>
            </Col>
            <Col lg={8}>
              <h5>{'Rp.' + data.amount + ',00'}</h5>
            </Col>
          </Row>
          <Row>
            <Col lg={4}>
              <h5>Status</h5>
            </Col>
            <Col lg={8}>
              <h5
                className={
                  data.status === 'SUCCESS' ? 'text-success' : 'text-danger'
                }>
                {data.status}
              </h5>
            </Col>
          </Row>
          <Row>
            <Col lg={4}>
              <h5>Waktu Transaksi</h5>
            </Col>
            <Col lg={8}>
              <h5>{data.timestamp}</h5>
            </Col>
          </Row>
          <Row>
            <Col lg={4}>
              <h5>Berita</h5>
            </Col>
            <Col lg={8}>
              <h5>{data.berita}</h5>
            </Col>
          </Row>
          <Row>
            <Col lg={12}>
              <p className="text-secondary">(Press Esc To Dismiss)</p>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

export default TransferModal;
