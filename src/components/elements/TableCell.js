import React from 'react';

const TableCell = props => {
  return <td className={props.classes}>{props.content}</td>;
};

export default TableCell;
